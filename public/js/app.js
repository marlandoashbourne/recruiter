//module for los app

var app = angular.module('myStarterApp', ['ngMessages', 'ui.bootstrap', 'ui.router']);

//angular configuration app routing
app.config(function($stateProvider,$urlRouterProvider,$locationProvider, $interpolateProvider){

  $urlRouterProvider.otherwise('/'); 

  $stateProvider.state('/',{
     url:'/',
       views : {
           '':{ 
                 templateUrl: 'js/templates/home.html',
                 controller: 'HomeController'
              }
       }
   })
   .state('new',{
          url:'/new',
          templateUrl: 'js/templates/new.html',
          controller:'HomeController'
   });
   
});
